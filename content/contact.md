---
title: "Get in contact"
linktitle: "Get in contact"
---


- **Address:** Rue de la Petite Île 1, 1070 Anderlecht, Brussels, Belgium.
- **Pidgeon:** 50.832593, 4.322160
    ([GoogleMaps](https://goo.gl/maps/NwCELYFNmXp))([OpenStreetMap](https://www.openstreetmap.org/search?query=hackerspace%20brussels#map=19/50.83233/4.32128))
- **Email:** [contact@hsbxl.be](mailto:contact@hsbxl.be)
- **Telephone:** [+32 2 880 40 04](tel:003228804004) (phone is in space)
- **EventPhone VPN Telephone:** [1070](tel:1070) (https://eventphone.de/doku/epvpn)
- **Facebook:** [HSBXL on Facebook](https://www.facebook.com/groups/hsbxl/)
- **Twitter:** [hsbxl](http://twitter.com/hsbxl) and [\#hsbxl](https://twitter.com/search?q=%23hsbxl)
- **Mastodon:** [@hsbxl@mastodon.social](https://mastodon.social/@hsbxl)
- **Matrix chat:** \#hsbxl on hackerspaces.be ([webchat](https://matrix.to/#/#hsbxl:matrix.org)) ([Join Matrix Space](https://matrix.to/#/#hackerspace-brussels:matrix.org))
- **IRC:** \#hsbxl on irc.libera.chat ([webchat](https://matrix.to/#/#hsbxl:matrix.org))
- **Mailing-list:** see the [ mailing-List](/mailinglist) page
- **Bank:** Argenta IBAN: BE69 9794 2977 5578; BIC: ARSP BE 22

If you are trying to get to HSBXL, please look [here](https://hsbxl.be/enter/) for a detailed description.

# Statutes at Belgisch Staatsblad / Moniteur Belge
- **Trade number**: BE0817617057
- **Official link to all statutes for HSBXL**: [website FOD Justice](http://www.ejustice.just.fgov.be/cgi_tsv/tsv_rech.pl?language=nl&btw=0817617057&liste=Liste)
- **2009-08-06**: [Statutes: Rubric Constitution (New Juridical Person, Opening Branch, etc...)](https://www.ejustice.just.fgov.be/tsv_pdf/2009/08/18/09118276.pdf)
- **2013-08-12**: [Statutes: Designation - Registered Office - Resignations - Appointments](https://www.ejustice.just.fgov.be/tsv_pdf/2013/08/22/13131033.pdf)
- **2016-05-10**: [Statutes: Registered Office - Resignations - Appointments](https://www.ejustice.just.fgov.be/tsv_pdf/2016/05/20/16069254.pdf)
- **2019-07-17**: [Statutes: Registered Office - Resignations - Appointments](https://www.ejustice.just.fgov.be/tsv_pdf/2019/07/24/19099868.pdf)
- **2022-01-10**: [Statutes: Resignations - Appointments](http://www.ejustice.just.fgov.be/tsv_pdf/2022/01/17/22007203.pdf)
