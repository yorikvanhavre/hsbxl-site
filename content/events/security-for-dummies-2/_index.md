---
startdate:  2022-06-25
starttime: "14:00"
endtime: "20:00"
linktitle: "Become anonymous with TAILS and TOR"
title: "Become anonymous with TAILS and TOR (FR-EN)"
price: "Free"
image: "tor.jpg"
series: "Security for dummies 2 : back with a vengeance"
eventtype: "Security for dummies 2 : back with a vengeance"
location: "HSBXL"
---

## Language
This workshop will be given in French and English.

## Workshop
This workshop aims to be an introduction to online privacy and security using the live-USB linux distribution TAILS (The Amnesiac Incognito Live System) and TOR. We will create bootable TAILS usb keys, learn some useful tips about how to protect your anonymity online and share some useful/interesting links accessible through TOR.

## Requirements
- Your trusty laptop
- A usb key with a minimum size of 8Gb (USB 3.0 and 16Gb or higher recommended)



Photo by RealToughCandy.com from Pexels
