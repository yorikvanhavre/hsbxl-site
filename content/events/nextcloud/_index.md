---
linktitle: "Nextcloud Workshops"
title: "Nextcloud Workshops"
location: "HSBXL"
eventtype: "workshop"
series: "BreakOpen"
start: "true"
startdate:  2023-05-09
starttime: "19:00"
endtime: "20:30"
---

Some Basic workshops on NextCloud