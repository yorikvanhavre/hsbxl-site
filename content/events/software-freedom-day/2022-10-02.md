---
startdate:  2022-10-02
starttime: "10:00"
endtime: "19:00"
linktitle: "Software Freedom Day 2022"
title: "Software Freedom Day 2022"
price: "Free"
image: "sfd.png"
series: "SFD"
eventtype: "workshops & talks"
location: "HSBXL"
---

# Principle
During Software Freedom Day, we'll be celebrating the merits of Free software (free as in Freedom, we ask you to pay for your beer). The main topic this year will be: The cloud is yours: What alternatives can be given to the big cloud players that better respect your personaly integrity?


# Live stream 
There's a live stream for those who can't make it. Try to come over though. There's more interaction.
https://youtu.be/kQ0aD6XOHqY

# Program

## How Bitwarden improves security and productivity (Jurgen)

* **Format:** Presentation - discussion
* **Prior knowledge:** no prior knowledge needed
* **Time slot:** 10:00 - 10:50

Last year, we decided we needed a better solution for shared passwords than the occasional spreadsheet and wiki page. This talk will talk about the process of choosing and implementing a pasword manager in your organisation. About opportunities, pitfalls and first results.

***There is no recording of this presentation***

[Slides of the presentation](/slides/2022/presentation.sozi.html)


**More information:**
* [Bitwarden website](https://bitwarden.com/)

## Knowledge management and problem solving through combining terse syntaxes (indietermancy)

* **Format:** Presentation - discussion
* **Prior knowledge:** Being comfortable using the linux command line
* **Time slot:** 11:00 - 11:50

The presentation presents core facets and functionalities of Icebreaker.
The project focuses on knowledge management and problem solving through combining terse syntaxes.

This includes:
* Gemtext (the Gemini protocols format)
* Emacs Hyperbole's Koutliner (hyperlinking format)
* Quiy (a recursive-modelling-language)

## I set up a cloud server in my internal home network - this is what I learned (Askarel)

* **Format:** Presentation - discussion
* **Prior knowledge:** no prior knowledge needed
* **Time slot:** 13:00 - 13:50

Basically, this would be a ten minute presentation with 40 minutes for Q&A.

## Get anonymous with Linux (Zipion)
* **Format:** Workshop
* **Prior knowledge:** no prior knowledge needed
* **Time slot:** 14:00 - 14:50
* **Bring along:** your computer + a USB stick (16GB or more)

This workshop aims to be an introduction to online privacy and security using the live-USB linux distributions TAILS (The Amnesiac Incognito Live System) and Whonix, and anonymous browsing with TOR. We will create bootable usb keys, install a virtual machine and learn some useful tips about how to protect your anonymity online and share some useful/interesting links accessible through TOR.

**More information:**
* [Tails website](https://tails.boum.org)
* [Whonix website](https://whonix.org)
* [TOR Browser](https://torproject.org)

## Using Nextcloud as a music streaming service (Kai)

* **Format:** Presentation - mini-workshop
* **Prior knowledge:** no prior knowledge needed
* **Time slot:** 15:00 - 15:50

Nextcloud is a simple solution to synchronize files of all sorts. While you can use it for documents and photo’s, it can also be used as a music streaming service. During this talk, I will give a brief introduction to Nextcloud and explain how I self host my personal music streaming service.


**More information:**
* [Nextcloud website](https://nextcloud.org)
* [Subsonic: your complete, personal media streamer](http://www.subsonic.org/pages/index.jsp)

## Communicating with your weather station the open way (Paul)

* **Format:** Presentation - discussion
* **Prior knowledge:** Basic understanding of dashboards and timeseries databases helps, but is not required
* **Time slot:** 16:00 - 16:50

How to gather metrics from a weatherstation and store them on your home network.

**More information:**
* weather station manufacturer: [Froggit](https://www.froggit.de)
* metrics collection: [Telegraf](https://www.influxdata.com/time-series-platform/telegraf/)
* dashboarding: [Grafana](https://grafana.com)
* Timeseries database: [InfluxDB](https://www.influxdata.com)

## EU Software Patents through the Unified Patent Court (Zoobab)

* **Format:** Presentation - discussion
* **Prior knowledge:** Basic understanding of software, and EU law
* **Time slot:** 17:00 - 17:50

The battle against software patenting in Europe is happening now. In July 2005, the proponents of software patentability agreed to drop the directive and push for the Unitary Patent and the Unified Patent Court (UPC) instead. 

**More information:**
* FFII: [FFII](https://www.ffii.org)
* The 3rd attempt to impose software patents in Europe via the UPC: [FFII UPC](https://ffii.org/ffii-oppose-the-third-attempt-to-impose-software-patents-in-europe-via-the-upc/)
* Unified Patent Court: [UPC Wikipedia](https://en.wikipedia.org/wiki/Unified_Patent_Court)


## Linux install party

* **Time slot:** 10:00 - 17:00 (bring along your computer)

Together with our friends at [BxLug](https://www.bxlug.be/), we'll also host a Linux install party. Bring along your old (or new) computer, and we'll help you install linux on it. Your machine will get a second life!

## Social gathering

* **Time slot:** 10:00 - 17:00 (just hop in!)

Have a talk and a drink with some members of the hackerspace. Get to know our space and our philosophy.

## Date
*Globally, Software Freedom Day gets celebrated on a different date. We're letting it coincide with the [Journée Découverte d'entreprise](https://www.jde-wallonie.be/) event at Cytigate to allow for a broader audience - on the date of **October 2nd, from 10:00 to 17:00** (and socially even longer).*

## Software Freedom Day Official Websites
* [Software Freedom Day main page](https://softwarefreedomday.org)
* [The wiki page pointing to our participation](https://wiki.softwarefreedomday.org/2022/Belgium/Brussels/Hackerspace%20Brussels)
