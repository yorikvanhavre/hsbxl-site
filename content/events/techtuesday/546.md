---
eventid: techtue546
startdate:  2019-10-29
starttime: "19:00"
linktitle: "TechTue 546"
title: "TechTuesday 546"
price: ""
image: "techtuesday.jpg"
series: "TechTuesday"
eventtype: "Social get-together"
location: "HSBXL"
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
