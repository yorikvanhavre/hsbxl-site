---
eventid: techtue514
startdate:  2019-03-19
starttime: "19:00"
linktitle: "TechTue 514"
title: "TechTuesday 514"
price: ""
image: "techtuesday.jpg"
series: "TechTuesday"
eventtype: "Social get-together"
location: "HSBXL"
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
