---
techtuenr: "673"
startdate: 2023-03-21
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue673
series: TechTuesday
title: TechTuesday 673
linktitle: "TechTue 673"
location: HSBXL
image: techtuesday.png
---
Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
