---
techtuenr: "662"
startdate: 2023-01-03
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue662
series: TechTuesday
title: TechTuesday 662
linktitle: "TechTue 662"
location: HSBXL
image: techtuesday.png
---
Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
