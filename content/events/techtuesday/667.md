---
techtuenr: "667"
startdate: 2023-02-07
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue667
series: TechTuesday
title: TechTuesday 667
linktitle: "TechTue 667"
location: HSBXL
image: techtuesday.png
---
Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
