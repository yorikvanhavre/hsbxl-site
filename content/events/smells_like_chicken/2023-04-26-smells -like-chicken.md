---
startdate:  2023-04-26
starttime: "14:00"
enddate:  2023-04-26
endtime: "16:00"
linktitle: "Soldering workshop: The basics"
title: "If it smells like chicken, you're holding it wrong"
price: "pay €7 for the soldering kit"
image: "beetle.png"
eventtype: "Workshop"
location: "HSBXL"
---

## Basic concepts of soldering

At the end of the workshop, you can take home your self soldered light sensitive beetle and learned a few simple concepts around electronics and soldering along the way.