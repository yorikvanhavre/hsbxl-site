---
startdate:  2023-01-31
starttime: "18:00"
enddate:  2023-01-31
endtime: "19:00"
linktitle: "Soldering workshop: The basics"
title: "If it smells like chicken, you're holding it wrong"
price: "pay €6 for the soldering kit"
image: "beetle.png"
eventtype: "Workshop"
location: "HSBXL"
series: "byteweek2023"
---

## Basic concepts of soldering

At the end of the workshop, you can take home your self soldered blinking beetle and learned a few simple concepts around electronics and soldering along the way.