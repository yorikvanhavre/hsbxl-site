---
linktitle: "Dataloss anonymous"
title: "Dataloss anonymous"
location: "HSBXL"
eventtype: "Cryptoparty"
series: "dataloss"
start: "true"
aliases: [dataloss]
---

    A bi-monthly "Dataloss anonymous' support group.
    Find closing on your data loss grief.
    We help each other preventing losing data again.
    

As we all know, there are  [5 stages in grief](https://grief.com/the-five-stages-of-grief).  
Anyone who has suffered (data) loss, knows these phases.
 
1. **Denial** - I didn't suffer any data loss, I'll for sure find some cloud that had my data...
2. **Anger** - It's all the fault of others! Stupid hard disk! Crappy laptop! Evil crook!
3. **Bargaining** - Just sent a personal mail to the NSA, please can you pull up my data?
4. **Depression** - It's all lost, you're ready to give up and never touch computers again in your life...
5. **Acceptance** - Accepting the pain of loss, allowing it to free up space on your hard disk to engage in new projects...



# Tools / solutions
Possible tools we'll investigate during the workshops

- [borg](https://borgbackup.readthedocs.io)
- [backuppc](https://backuppc.github.io/backuppc)
- [backintime](https://github.com/bit-team/backintime)
- [Déjà Dup](https://gitlab.gnome.org/World/deja-dup)
- [Ccollect](https://www.nico.schottelius.org/software/ccollect/)
- [inotify/rsync](https://linuxhint.com/inotofy-rsync-bash-live-backups)

If you want to suggest/add more tools,  
edit this page on the bottom, just add for yourself.


## Upcoming 'Dataloss Anonymous' groups
{{< events when="upcoming" series="dataloss" >}}

## Past 'Dataloss Anonymous" groups
{{< events when="past" series="dataloss" >}}