---
linktitle: "Cleaning up"
title: "Cleaning up the space"
image: "cleanup"

---

A get together with kids, teens and parents to work on tech projects and learn new stuff.

## Upcoming events
{{< events when="upcoming" series="Cleanup" >}}

## Past events
{{< events when="past" series="Cleanup" >}}
